import * as request from "request-promise-native";
import * as _ from "underscore";

// gitlab token with API access
const token: string = process.env.GITLAB_TOKEN;
if (typeof token !== "string")
    throw new Error("Environment variable GITLAB_TOKEN not found!");

// the runnerIds you want to attach to all your projects
let globalRunnerIdsString: string = process.env.GITLAB_RUNNER_IDS;
if (typeof globalRunnerIdsString !== "string")
    throw new Error("Environment variable GITLAB_RUNNER_IDS not found!");
let globalRunnerIds: number[] = []
for (let runnerId of globalRunnerIdsString.split(",")) {
    try {
        globalRunnerIds.push(parseInt(runnerId));
    } catch (e) {
        throw new Error("Not a runner ID: " + runnerId + ", Error: " + e);
    }
}

// the gitlab group name
const groupName = process.env.GITLAB_GROUP_NAME;

if (typeof groupName !== "string")
    throw new Error("Environment variable GITLAB_GROUP_NAME not found!");

const standardOptions: any = {
    headers: {
        "PRIVATE-TOKEN": token
    },
    json: true
}

async function start() {
    console.log("getting group...");
    let groups: any[] = await request.get("https://gitlab.com/api/v4/groups?search=" + groupName, standardOptions);
    if (!groups || groups.length === 0)
        throw new Error("Group not found!");
    if (groups.length > 1)
        throw new Error("More than one group found!");
    console.log(`getting subgroups for group "${groups[0].name}"...`);
    const subGroups = await request.get(`https://gitlab.com/api/v4/groups/${groups[0].id}/subgroups`, standardOptions);
    if (subGroups && subGroups.length > 0)
        groups = groups.concat(subGroups);

    let projects: any[] = [];
    for (let group of groups) {
        const groupId: number = group.id;
        console.log("Found group: " + group.full_name + ", ID: " + group.id);

        console.log("getting projects...");
        projects = projects.concat(await request.get("https://gitlab.com/api/v4/groups/" + groupId + "/projects?simple=true&per_page=100", standardOptions));
    }
    console.log("found " + projects.length + " projects...");

    console.log("deleting obsolete runners...")
    const allRunners = await request.get("https://gitlab.com/api/v4/runners", standardOptions);
    for (const runner of allRunners) {
        if (globalRunnerIds.indexOf(runner.id) !== -1) {
            console.log("Skipping runner with ID " + runner.id);
            continue;
        }

        const deleteRunnersFromProjectsPromises = [];
        for (const project of projects) {
            deleteRunnersFromProjectsPromises.push(new Promise<any>(async (resolve) => {
                try {
                    console.log("Removing runner with ID " + runner.id + " from project " + project.name + "...");
                    await request.del("https://gitlab.com/api/v4/projects/" + project.id + "/runners/" + runner.id, standardOptions);
                } catch (e) {
                    // console.error(e.error);
                }
                resolve();
            }));
        }
        await Promise.all(deleteRunnersFromProjectsPromises);


        console.log("Deleting runner with ID " + runner.id);
        try {
            await request.del("https://gitlab.com/api/v4/runners/" + runner.id, standardOptions);
        } catch (e) {
            console.error(e.error);
        }
    }

    console.log("attaching globalRunners to all projects...");
    const attachGlobalRunnersPromises = [];
    for (const globalRunnerId of globalRunnerIds) {
        for (const project of projects) {
            attachGlobalRunnersPromises.push(new Promise<any>(async (resolve) => {
                try {
                    console.log("attaching " + globalRunnerId + " to " + project.name);
                    await request.post("https://gitlab.com/api/v4/projects/" + project.id + "/runners", _.extend(standardOptions, {
                        form: {
                            runner_id: globalRunnerId
                        }
                    }));
                } catch (e) {
                    console.error(e.error);
                }
                resolve();
            }));
        }
    }

    await Promise.all(attachGlobalRunnersPromises);
}


start();